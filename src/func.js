const getSum = (str1, str2) => {
  str1 = typeof str1 === 'string' && str1.length === 0?'0':str1;
  str2 = typeof str2 === 'string' && str2.length === 0?'0':str2;
   let sum;
    if(typeof str1 !== 'string' || typeof str2 !== 'string' ){
      return false;
    }else if(/^\d+$/.test(str1) !==true || /^\d+$/.test(str2) !==true){
       return false;
    }else{
     // eslint-disable-next-line no-undef
     sum = BigInt(str1) + BigInt(str2);
     return sum.toString();
    }
  };

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
    let postsCount = 0;
    let commentsCount = 0;
    for (let post of listOfPosts){
    if(post.author === authorName){
           postsCount++
      }
         if(post.comments){
           post.comments.forEach(el => {
             if(el.author === authorName){
                commentsCount++;
             }
           })
         }
    }
       return `Post:${postsCount},comments:${commentsCount}`;
};

const tickets=(people) => {
  let price = 25;
  let count25 = 0;
  let count50 = 0;
  let fifty = 50;
  let hundred = 100;
  let three = 3;
  for(let person of people){
    if(Number(person) === price){
     count25++;
    }else if(Number(person) === fifty&&count25>0){
     count50++;
     count25--;
    } else if(Number(person) === hundred){
      if(count50>0&&count25>0){
       count25--;
       count50--;
      }else if(count25>=3){
       count25-=three;
      }else{
      return 'NO';
      }
    }
  }
  return 'YES';
  };


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
